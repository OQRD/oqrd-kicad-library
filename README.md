# OQRD KiCAD Library

### Installation

##### Install KiCAD:

See http://www.kicad-pcb.org/

On Ubuntu: `sudo apt-get install kicad`
On Arch: `sudo pacman -S kicad`

##### Set up relative path:

In KiCAD, go to Preferences>Configure Paths...>Add

Under _name_ put **OQRD_DIR**

Under _value_ set it to `/path/to/root/dir`

##### Set up the Schematic Library

In the _Schematic Editor_ (EESchema), go to Preferences>Manage symbol libraries...

Click on Append Library and add `${OQRD_DIR}/oqrd-kicad-library/oqrd-kicad.lib`

##### Set up Footprint Library

In the _Footprint Editor_ (PCBNew), go to Preferences>Manage Footprints Library

Click on Append Library and add `insert path here`

> Note: there currently are no custom footprints, when the footprint library is created this will be necessary.
